import React, { Component } from 'react';

export default class ImportFilter extends Component {
  render() {
    return (
      <div className='btn-group btn-group-toggle d-flex justify-content-end'>
        <button className='btn btn-info' type='button'>All</button>
        <button className='btn btn-outline-secondary' type='button'>Active</button>
        <button className='btn btn-outline-secondary' type='button'>Done</button>
      </div>
    );
  };
}