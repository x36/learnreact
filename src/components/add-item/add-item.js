import React, { Component } from 'react';

export default class AddItem extends Component {
  state = {
    label: ''
  };

  onLabelChange = (evt) => {
    this.setState({
      label: evt.target.value
    });
  };

  onSubmit = (evt) => {
    evt.preventDefault();
    this.props.onAdd(this.state.label);
    this.setState({
      label: ''
    });
  };

  render() {
    return (
      <form
        className='input-group'
        onSubmit={this.onSubmit}>
        <input
          type='text'
          className='form-control input-group-prepend'
          onChange={this.onLabelChange}
          placeholder='New task...'
          value={this.state.label} />
        <button
          className='btn btn-primary input-group-append'>
          Add Item</button>
      </form >
    );
  };
};
