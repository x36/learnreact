import React from 'react';

const SearchPanel = () => {
  const searchText = 'Type here to search...';
  const searchStyle = {
    fontSize: '18px'
  };

  return (
    <div className='input-group d-flex'>
      <div className='input-group-prepend'>
        <input style={searchStyle}
          placeholder={searchText}
          className='form-control'>
        </input>
      </div>
      <div className='input-group-append'>
        <button className='btn btn-outline-secondary' type='button'>
          <i className='fa fa-search'></i>
        </button>
      </div>
    </div>
  );
};

export default SearchPanel;