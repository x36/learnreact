import React, { Component } from 'react';

import Header from '../app-header';
import Search from '../search-panel';
import Filter from '../item-status-filter';
import List from '../todo-list';
import AddItem from '../add-item';

export default class App extends Component {
  maxId = 100;

  state = {
    todoData: [
      { label: 'Drink Coffee', important: false, id: 1 },
      { label: 'Learn React', important: true, id: 2 },
      { label: 'Get a haircut', important: false, id: 3 }
    ]
  };

  addItem = (text) => {
    const newItem = {
      label: text,
      important: false,
      id: this.maxId++
    };

    this.setState(({ todoData }) => {
      const newArray = [
        ...todoData, newItem
      ];
      return {
        todoData: newArray
      };
    });
  };

  deleteItem = (id) => {
    this.setState(({ todoData }) => {
      const idx = todoData.findIndex((el) => el.id === id);

      const newArray = [
        ...todoData.slice(0, idx), ...todoData.slice(idx + 1)
      ];

      return {
        todoData: newArray
      };
    });
  };

  onToggleImportant = (id) => {
    console.log('Toggle Impoertant', id);
  };

  onToggleDone = (id) => {
    console.log('Toggle Done', id);
  };

  render() {
    return (
      <div className='container' >
        <Header />
        <div className='d-flex pb-3'>
          <Search />
          <Filter />
        </div>
        <List
          todos={this.state.todoData}
          onDeleted={this.deleteItem}
          onToggleImportant={this.onToggleImportant}
          onToggleDone={this.onToggleDone} />
        <AddItem onAdd={this.addItem} />
      </div>
    );
  };
};
