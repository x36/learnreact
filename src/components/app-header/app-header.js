import React from 'react';

const AppHeader = () => {
  return (
    <div className='d-flex justify-content-between'>
      <h1>ToDo List</h1>
      <p className='align-self-end'>
        <span>1</span> more to do, <span>2</span> done
      </p>
    </div>
  );
};

export default AppHeader;