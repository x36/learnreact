import React from 'react';

const SearchPanel = () => {
  const searchText = 'Type here to seatch';
  const searchStyle = {
    fontSize: '18px'
  };
  return (
    <input
      style={searchStyle}
      placeholder={searchText}
      className="search">
    </input>
  )
};

export default SearchPanel;