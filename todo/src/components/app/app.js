import React from 'react';

import Header from '../app-header';
import Search from '../search-panel';
import List from '../todo-list';

const App = () => {
  const todoData = [
    { label: 'Drink Coffee', important: false, id: 1 },
    { label: 'Learn React', important: true, id: 2 },
    { label: 'Get a haircut', important: false, id: 3 }
  ]
  return (
    <div>
      <Header />
      <Search />
      <List todos={todoData} />
    </div>
  );
};

export default App;